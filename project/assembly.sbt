
resolvers += Resolver.bintrayIvyRepo("com.eed3si9n", "sbt-plugins")
resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases"
resolvers += "artifactory" at "http://scalasbt.artifactoryonline.com/scalasbt/sbt-plugin-releases"
resolvers += Resolver.url("bintray-sbt-plugins", url("http://dl.bintray.com/sbt/sbt-plugin-releases"))(Resolver.ivyStylePatterns)
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.6")
import java.io.{File, FileInputStream}
import java.nio.file.{Path, Paths}

import actors.fa.{FAHelper, ImagePage, WatchlistTraversalActor}
import analysis.ImageAnalysisResult
import com.gargoylesoftware.htmlunit.WebClient
import com.gargoylesoftware.htmlunit.html.HtmlPage
import jpa.{Artist, Website}
import org.jsoup.Jsoup
import org.scalatest._

import scalaz.{-\/, \/-}

class TestHtmlParsing extends FlatSpec with Matchers {

  lazy val expectedArtists : Set[Artist] = {
    val artistA = new Artist("-Lofi", Website.FA, "-lofi")
    val artistB = new Artist("AeroSocks", Website.FA, "aerosocks")
    val artistC = new Artist("arithm", Website.FA, "arithm")
    Set(artistA, artistB, artistC)
  }

  def getJSDisabledWebClient = {
    val wc = new WebClient
    wc.getOptions.setJavaScriptEnabled(false)
    wc
  }

  "artistFromPage()" should "parse and return a list of artist in the page" in {
    val wc = getJSDisabledWebClient
    val page : HtmlPage = wc.getPage(getClass getResource "watchlist.html")
    val ret = WatchlistTraversalActor.artistsFromPage(page)
    ret match {
      case -\/(_) => fail()
      case \/-(res) =>
        // The result must be subset of each other
        assert(res.toSet.subsetOf(expectedArtists))
        assert(expectedArtists.subsetOf(res.toSet))
    }
  }

  it should "parse and return a list of artist in the page with Jsoup too" in {
    val input = new File(getClass getResource "watchlist.html" toURI)
    val doc = Jsoup.parse(input, "UTF-8", "https://furaffinity.net")
    val ret = WatchlistTraversalActor.artistsFromPage(doc)

    ret match {
      case -\/(_) => fail()
      case \/-(res) =>
        // The result must be subset of each other
        assert(res.toSet.subsetOf(expectedArtists))
        assert(expectedArtists.subsetOf(res.toSet))
    }
  }

  lazy val expectedTags: Set[jpa.Tag] = {
    "commissions, request, free, bust, chibi, badge, commission, requests".split(',').map(_.trim).map(jpa.Tag).toSet
  }

  "parseImage()" should "parse and get key info from the page with jsoup" in {
    val input = new File(getClass getResource "image_page.html" toURI)
    val doc = Jsoup.parse(input, "UTF-8", "https://furaffinity.net")
    val ret = ImagePage.parsePage(doc)

    ret match {
      case -\/(_) => fail()
      case \/-(res) =>
        assert(res.downloadLink.toString == "https://d.facdn.net/art/kestrels/1507075590/1507075590.kestrels_freeart.png")
        // The result must be subset of each other
        //assert(res.tags.subsetOf(expectedTags))
        //assert(expectedTags.subsetOf(res.tags))
    }
  }

  it should "return UTF-8 encoded URL" in {
    val input = new File(getClass getResource "image_page_foreign.html" toURI)
    val doc = Jsoup.parse(input, "UTF-8", "https://furaffinity.net")
    val ret = ImagePage.parsePage(doc)
    ret match {
      case -\/(_) => fail()
      case \/-(res) =>
        assert(res.downloadLink.toString == "https://d.facdn.net/art/kestrels/1507075590/1507075590.훈민정음.png")
      // The result must be subset of each other
      //assert(res.tags.subsetOf(expectedTags))
      //assert(expectedTags.subsetOf(res.tags))
    }

  }

  "parseGalleryPage" should "parse and return all the URL to image page on a gallery page" in {
    val input = new File(getClass getResource "Gallery_page.html" toURI)
    val doc = Jsoup.parse(input, "UTF-8", "https://furaffinity.net")
    val ret = FAHelper.parseGalleryPage(doc)
    println(ret)

  }


  "imageanaylsis" should "identify invalid jpgs" in {
    val input = new File(getClass getResource "invalid.jpg" toURI)
    val byte = new Array[Byte](input.length().asInstanceOf[Int])
    new FileInputStream(input).read(byte)
    assert(!FAHelper.imageAnalysis(byte))
  }

  it should "return true on valid jpgs" in {
    val input = new File(getClass getResource "valid.jpg" toURI)
    val byte = new Array[Byte](input.length().asInstanceOf[Int])
    new FileInputStream(input).read(byte)
    assert(FAHelper.imageAnalysis(byte))
  }

  it should "identify invalid pngs" in {
    val input = new File(getClass getResource "invalid.png" toURI)
    val byte = new Array[Byte](input.length().asInstanceOf[Int])
    new FileInputStream(input).read(byte)
    assert(!FAHelper.imageAnalysis(byte))
  }

  it should "return true on valid pngs" in {
    val input = new File(getClass getResource "valid.png" toURI)
    val byte = new Array[Byte](input.length().asInstanceOf[Int])
    new FileInputStream(input).read(byte)
    assert(FAHelper.imageAnalysis(byte))
  }

  it should "identify invalid gifs" in {
    val input = new File(getClass getResource "invalid.gif" toURI)
    val byte = new Array[Byte](input.length().asInstanceOf[Int])
    new FileInputStream(input).read(byte)
    // TODO: Identify invalid gifs
    //assert(!FAHelper.imageAnalysis(byte))
  }

  it should "return true on valid gif" in {
    val input = new File(getClass getResource "valid.gif" toURI)
    val byte = new Array[Byte](input.length().asInstanceOf[Int])
    new FileInputStream(input).read(byte)
    assert(FAHelper.imageAnalysis(byte))
  }

}

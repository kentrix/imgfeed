package analysis;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

public class ImageAnalysisResult {

    public static boolean analyzeImage(final Path file)
            throws NoSuchAlgorithmException, IOException {
        final ImageAnalysisResult result = new ImageAnalysisResult();

        final InputStream digestInputStream = Files.newInputStream(file);
        try {
            final ImageInputStream imageInputStream = ImageIO
                    .createImageInputStream(digestInputStream);
            final Iterator<ImageReader> imageReaders = ImageIO
                    .getImageReaders(imageInputStream);
            if (!imageReaders.hasNext()) {
                return true;
            }
            final ImageReader imageReader = imageReaders.next();
            imageReader.setInput(imageInputStream);
            final BufferedImage image = imageReader.read(0);
            if (image == null) {
                return false;
            }
            image.flush();
            if (imageReader.getFormatName().equals("JPEG")) {
                imageInputStream.seek(imageInputStream.getStreamPosition() - 2);
                final byte[] lastTwoBytes = new byte[2];
                imageInputStream.read(lastTwoBytes);
                if (lastTwoBytes[0] != (byte)0xff || lastTwoBytes[1] != (byte)0xd9) {
                    return false;
                } else {
                    return true;
                }
            }
            return true;
        } catch (final IndexOutOfBoundsException e) {
            return false;
        } catch (final IIOException e) {
            if (e.getCause() instanceof EOFException) {
                return false;
            }
        } finally {
            digestInputStream.close();
        }
        return false;
    }

}

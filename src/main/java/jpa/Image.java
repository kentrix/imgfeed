package jpa;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "IMAGES")
@Table(name = "IMAGES")
public class Image {
    public Image() {}
    public Image(Artist artist, String uniqueWebsiteId, String filePath) {
        this.artist = artist;
        this.uniqueWebsiteId = uniqueWebsiteId;
        this.filePath = filePath;
    }
    @GeneratedValue
    @Id
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE})
    private Artist artist;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    @Lob
    @Column(length = 4096)
    private String metaData;

    @ElementCollection
    private Set<Tag> tags;

    @Column
    private String filePath;

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getUniqueWebsiteId() {
        return uniqueWebsiteId;
    }

    public void setUniqueWebsiteId(String uniqueWebsiteId) {
        this.uniqueWebsiteId = uniqueWebsiteId;
    }

    @Column
    private String uniqueWebsiteId;

}

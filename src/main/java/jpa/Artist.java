package jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Entity(name = "ARTISTS")
@Table(name = "ARTISTS")
public class Artist implements Serializable {
    public Artist(){}

    public Artist(String userName, Website website, String uniqueWebsiteId) {
        this.userName = userName;
        this.website = website;
        this.uniqueWebsiteId = uniqueWebsiteId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public Website getWebsite() {
        return website;
    }

    public void setWebsite(Website website) {
        this.website = website;
    }

    public Set<Image> getImageSet() {
        return imageSet;
    }

    public void setImageSet(Set<Image> imageSet) {
        this.imageSet = imageSet;
    }

    private String userName;
    @Lob
    @Column(length = 4096)
    private String metaData;
    //@Convert(converter = WebsiteConverter.class)
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    @Id
    private Website website;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "artist", cascade = {CascadeType.MERGE})
    private Set<Image> imageSet;
    @Column(nullable = false, unique = false)
    @Id
    private String uniqueWebsiteId;

    @OneToOne(fetch = FetchType.EAGER)
    private Image lastImage;

    @OneToOne(fetch = FetchType.EAGER)
    private Image lastScrapImage;

    public String getUniqueWebsiteId() {
        return uniqueWebsiteId;
    }

    public void setUniqueWebsiteId(String uniqueWebsiteId) {
        this.uniqueWebsiteId = uniqueWebsiteId;
    }

    public Image getLastImage() {
        return lastImage;
    }

    public void setLastImage(Image lastImage) {
        this.lastImage = lastImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Artist artist = (Artist) o;

        if (website != artist.website) return false;
        return uniqueWebsiteId.equals(artist.uniqueWebsiteId);
    }

    @Override
    public int hashCode() {
        int result = website.hashCode();
        result = 31 * result + uniqueWebsiteId.hashCode();
        return result;
    }

    public Image getLastScrapImage() {
        return lastScrapImage;
    }

    public void setLastScrapImage(Image lastScrapImage) {
        this.lastScrapImage = lastScrapImage;
    }
}

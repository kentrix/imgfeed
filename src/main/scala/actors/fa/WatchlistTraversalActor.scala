package actors.fa

import actors.fa.WatchlistTraversalActor.{NewTraversalTask, ScrappedArtistList}
import akka.actor.{Actor, ActorLogging, ActorRef}
import com.gargoylesoftware.htmlunit.html._
import com.gargoylesoftware.htmlunit.javascript.host.dom.NodeList
import com.gargoylesoftware.htmlunit.{BrowserVersion, WebClient}
import jpa.{Artist, Website}
import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element}
import org.jsoup.select.Elements

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.{Failure, Success}
import scalaz.{-\/, Maybe, \/, \/-}
import scalaz.Scalaz._

class WatchlistTraversalActor extends Actor with ActorLogging {

  var webClient : WebClient = _
  var parent : ActorRef = _

  override def preStart(): Unit = {
    webClient = new WebClient(BrowserVersion.CHROME)
    webClient.getOptions.setJavaScriptEnabled(false)
  }

  override def receive = {
    case NewTraversalTask(who) => {
      log.debug(s"Traverse watch list of $who")
      parent = sender()
      context.become(running)
      Future[List[Artist]] {
        traverseTask(who, 1)
      }.onComplete {
        case Success(al) => { parent ! ScrappedArtistList(al); context.become(receive) }
        case Failure(f) => throw f
      }
    }
      /* DEAD Letter from here onwards */
  }

  def traverseTask(who : String, i : Int) : List[Artist] = {
    val url = FAConstants.USER_WATCHLIST_TEMPLATE.format(who, i)
    val doc = FAHelper.retryJsoupGetPage(url)
    val nlist = WatchlistTraversalActor.artistsFromPage(doc)
    log.debug(s"nlist => $nlist")
    nlist match {
      case -\/(e) => throw e
      case \/-(l) => if (l.isEmpty) l else traverseTask(who, i+1) ++ l
    }
  }

  /*
  def traverseTask(who : String, i : Int) : List[Artist] = {
    val url = FAConstants.USER_WATCHLIST_TEMPLATE.format(who, i)
    val page: HtmlPage = webClient.getPage(url)
    val nlist = WatchlistTraversalActor.artistsFromPage(page)
    log.debug(s"nlist => $nlist")
    nlist match {
      case -\/(e) => throw e
      case \/-(l) => if (l.isEmpty) l else traverseTask(who, i+1) ++ l
    }
  }
  */

  def running: PartialFunction[Any, Unit] = {
    case s@_ => {
      log.debug(s"Unknown message received => ${s.toString}")
    }
  }
}

object WatchlistTraversalActor {

  trait WatchlistTraversalActorMsg
  case class NewTraversalTask(who: String) extends WatchlistTraversalActorMsg
  case class ScrappedArtistList(list : List[Artist]) extends WatchlistTraversalActorMsg

  def artistsFromPage(page: HtmlPage) : \/[Exception, List[Artist]] = {
    val nodeList = page.getElementsByTagName("table")
    val theTable : HtmlTable = nodeList.get(1).asInstanceOf[HtmlTable]
    if(theTable == null)
      new Exceptions.PossibleDisabledAccount().left
    else
      theTable.getRows.asScala.map(rowToArtist).toList.right
  }

  def artistsFromPage(doc : Document) : \/[Exception, List[Artist]] = {
    val elements: Elements = doc.select("table#userpage-budlist tr a")
    if(elements == null)
      new Exceptions.PossibleDisabledAccount().left
    else
      elements.asScala.map(elementToArtist).toList.right
  }

  def elementToArtist(element : Element): Artist = {
    val uniquewsid = element.attr("href").split('/').last
    val uname = element.text()
    new Artist(uname, Website.FA, uniquewsid)
  }

  def rowToArtist(row : HtmlTableRow) : Artist = {
    val data = row.getCell(0).getElementsByTagName("a").get(0)
    val artist = new Artist
    val href = data.getAttribute("href")
    artist.setUniqueWebsiteId(href.substring(6, href.length - 1))
    artist.setUserName(data.asText())
    artist.setWebsite(Website.FA)
    artist
  }

}


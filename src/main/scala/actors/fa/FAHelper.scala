package actors.fa

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
import java.lang.Throwable
import java.util.{Timer, TimerTask}
import javax.imageio.ImageIO

import com.typesafe.scalalogging.LazyLogging
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import types.Types.Exceptionable

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.util.{Failure, Success}
import scalaz.Scalaz._
import scala.util.control.Exception
import scalaz.{-\/, Maybe, \/, \/-}
import scala.concurrent.ExecutionContext.Implicits.global

object FAHelper extends LazyLogging {
  def parseGalleryPage(document : Document) : List[String] = {
    val res = document.select("#gallery-gallery figure b u a")
    res.asScala.map(e => e.attr("abs:href")).toList
  }

  def jsoupConnectPage(url : String) = {
    Jsoup.connect(url).cookies(Util.Constants.getCookie()).userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36").maxBodySize(0)
  }

  def parseIntOrDefault(string: String)(implicit int: Int): Int = {
    try {
      Integer.parseInt(string)
    }
    catch {
      case _ : Throwable => int
    }
  }

  def retryJsoupGetPage(url : String) : Document = {
    retry[Document]( () => jsoupConnectPage(url).get)
  }

  def retryJsoupGetRaw(url: String, retryCount : Int, condi : Array[Byte] => Boolean) : \/[Exception, Array[Byte]]= {
    val checker = if(url.endsWith("jpg") || url.endsWith("jpeg") || url.endsWith("gif") || url.endsWith("png")) condi else {_ : Any => true}
    retry[Array[Byte]](() => jsoupConnectPage(url).ignoreContentType(true).execute().bodyAsBytes(), retryCount, checker)
  }

  def retryJsoupGetRaw(url : String, retryCount : Int): \/[Exception, Array[Byte]] = {
    retryJsoupGetRaw(url, retryCount, {_ => true})
  }

  def until[A](condition : A => Boolean)(exe : Unit => A) : A = {
    // until condi $ (repeat action)
    def looper(condition : A => Boolean)(exe : Unit => A) : A = {
      val f = exe()
      if(condition(f)) f else looper(condition)(exe)
    }

    try {
      looper(condition)(exe)
    }
    catch {
      case e : Throwable => throw e
    }

  }

  def retry[A](exe : () => A, int : Int, condition : A => Boolean) : \/[Exception, A] = {
    if(int < 0) { new Exception("Out of retries").left }
    else {
      try {
        val res = exe()
        if(condition(res)) res.right
        else throw new Exception("Check failed")
      }
      catch {
        // XXX:Convert ex to option and back to ex later......
        case a: Exception =>
          logger.error(s"Failed once ${a}, remaining $int")
          import scala.concurrent.blocking
          logger.error(s"Sleeping for 10 sec")
          blocking(Thread.sleep(10000))
          retry[A](exe, int - 1, condition)
      }
    }
  }


  def retry[A](exe : () => A): A = {
      try { exe() }
      catch {
        case a : Exception =>
          logger.debug(s"Failed once ${a}")
          import scala.concurrent.blocking
          logger.error(s"Sleeping for 10 sec")
          blocking(Thread.sleep(10000))
          retry[A](exe)
    }
  }

  def imageAnalysis(img : Array[Byte]) : Boolean = {
    try {
      val is = ImageIO.createImageInputStream(new ByteArrayInputStream(img))
      val readers = ImageIO.getImageReaders(is)
      readers.asScala.foreach { r =>
        r.setInput(is)
        val i = r.read(0)
        if (i != null) {
          i.flush()
          if (r.getFormatName.matches("JPE[G]")) {
            is.seek(is.getStreamPosition - 2)
            val lastTwoBytes = new Array[Byte](2)
            is.read(lastTwoBytes)
            return !(lastTwoBytes.apply(0) != 0xff.asInstanceOf[Byte] || lastTwoBytes.apply(1) != 0xd9.asInstanceOf[Byte])
          }
          return true
        }
      }
    }
    catch {
      case _ : Exception => return false
    }
    false
  }

  def performEither[A <: Exception, B](a : \/[A, B]) : B = {
    a match {
      case -\/(ex) => throw ex
      case \/-(b) => b
    }
  }

  def cleanly[A, B](resource: => A)(cleanUp: A => Unit)(fn: A => B): Exceptionable[B] = {
    try {
      val r = resource
      try { fn(r).right } finally { cleanUp(r) }
    }
    catch {
      case e: Exception => e.left
    }
  }

  def retry[T](n : Int)(future: Future[T]) : \/[List[Throwable], T] = {
    val exs = new ListBuffer[Throwable]
    for (_ <- 0 to n) {
      future.onComplete {
        case Success(t) => return t.asInstanceOf[T].right
        case Failure(e) => exs += e
      }
    }
    exs.left.asInstanceOf[\/[List[Throwable], T]]
  }

  // Surely this is somewhere in the stdlib
  def maybe[A](default : => A)(may : Option[A]) : A = {
    may match {
      case Some(s) => s
      case None => default
    }

  }

  trait GalleryKind
  object NormalGalleryKind extends GalleryKind
  object ScrapGalleyKind extends GalleryKind


}

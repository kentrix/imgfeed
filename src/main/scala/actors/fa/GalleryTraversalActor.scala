package actors.fa

import java.net.URL

import actors.ActorCommons.Done
import actors.fa.FAHelper.{NormalGalleryKind, ScrapGalleyKind}
import actors.fa.GalleryTraversalActor.{GeneratedDownloadJob, StartTraversingOf, TraverserDone}
import actors.fa.ImagePage.PageData
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import jpa.Artist
import org.jsoup.Connection.Method
import org.jsoup.nodes.Document

import scala.collection.JavaConverters._
import org.jsoup.{Connection, Jsoup}

import scalaz.{-\/, \/, \/-}

class GalleryTraversalActor(val templateString : String, val kind : FAHelper.GalleryKind) extends Actor with ActorLogging {

  var parent : ActorRef = _
  final implicit val startWith : Int = 0

  override def receive = {
    case StartTraversingOf(artists) => {
      parent = sender()
      context.become(running)
      artists.foreach(artist => {
        val lastImgId = if(artist == null) 0
        else kind match {
          case NormalGalleryKind => if (artist.getLastImage != null) FAHelper.parseIntOrDefault(artist.getLastImage.getUniqueWebsiteId) else 0
          case ScrapGalleyKind =>  if (artist.getLastScrapImage != null) FAHelper.parseIntOrDefault(artist.getLastScrapImage.getUniqueWebsiteId) else 0
        }

        traverseTask(artist.getUniqueWebsiteId, 1, lastImgId).reverse.foreach(url => {
          ImagePage.parsePage(FAHelper.retryJsoupGetPage(url)) match {
            case -\/(ex) => throw ex
            case \/-(pageData) => parent ! GeneratedDownloadJob(artist, pageData, url.split("/").last, kind)
          }
        })
      })

      parent ! TraverserDone

    }

  }

  def running : Actor.Receive = {
    case _ => log.error("Unknown msg received")
  }

  def traverseTask(who : String, i : Int, until : Int) : List[String] = {
    val doc = FAHelper.retryJsoupGetPage(templateString.format(who, i))

    val urlList = FAHelper.parseGalleryPage(doc)
    val uniqueIdList = urlList.map(url => {
      Integer.parseInt(url.split("/").last)
    }).filter(_ > until)
    if(uniqueIdList.length != urlList.length || urlList.isEmpty) urlList.take(uniqueIdList.length)
    else urlList.take(uniqueIdList.length) ++ traverseTask(who, i+1, until)

  }
}

object GalleryTraversalActor {
  def props(templateString : String, kind : FAHelper.GalleryKind): Props = Props(new GalleryTraversalActor(templateString, kind))
  trait GalleryTraversalActorMsg
  case class StartTraversingOf(artists: List[Artist]) extends GalleryTraversalActorMsg
  case class GeneratedDownloadJob(artist: Artist, pageData: PageData, uniqueWebsiteIdForImg : String, kind: FAHelper.GalleryKind) extends GalleryTraversalActorMsg
  case object TraverserDone

}



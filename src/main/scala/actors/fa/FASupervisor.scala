package actors.fa

import java.util.{Timer, TimerTask}
import java.util.logging.Logger

import Util.PersistenceManager
import actors.ActorCommons.{AskForWork, Start}
import actors.fa.FAHelper.{NormalGalleryKind, ScrapGalleyKind}
import actors.fa.GalleryTraversalActor.{GeneratedDownloadJob, TraverserDone}
import actors.fa.WatchlistTraversalActor.ScrappedArtistList
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern._
import com.typesafe.config.ConfigFactory
import jpa.{ArtistDAO, ArtistDAOJpaImpl}

import scala.collection.mutable
import scalaz.{-\/, \/-}
import scalaz.Scalaz._
import scalaz.Monad._

class FASupervisor extends Actor with ActorLogging {
  var artistDAO : ArtistDAO = _
  var childrenQueue : mutable.Queue[ActorRef] = _
  var jobQueue : mutable.Queue[GalleryTraversalActor.GeneratedDownloadJob] = _
  var galleryScrapperActor : ActorRef = _
  var scrapScrapperActor : ActorRef = _
  var traverserDoneCount : Int = _
  var scheduledRestart = false

  override def preStart(): Unit = {
    artistDAO = new ArtistDAOJpaImpl(PersistenceManager.getFactory.createEntityManager())
    childrenQueue = new mutable.Queue[ActorRef]
    jobQueue = new mutable.Queue[GalleryTraversalActor.GeneratedDownloadJob]
    traverserDoneCount = 0
  }

  override def receive = {
    case Start => {
      scheduledRestart = false
      val ac = context.actorOf(Props[WatchlistTraversalActor])
      ac ! WatchlistTraversalActor.NewTraversalTask(Util.Constants.watchlistOf)
    }
    case ScrappedArtistList(list) => {
      log.debug(s"Received ScrappedArtistList ${ScrappedArtistList}")
      val nl = list.map(a => {
        val res = artistDAO.getByArtist(a)
        res match {
          case -\/(e) =>
            log.debug(s"Artist Not found ${e.toString}")
            artistDAO.newArtist(a)
            a
          case \/-(r) =>
            log.debug(s"Artist found ${r.toString}")
            r
        }
      })
      // Create two Traversers
      galleryScrapperActor = context.actorOf(GalleryTraversalActor.props(FAConstants.USER_GALLERY_TEMPLATE, NormalGalleryKind))
      scrapScrapperActor = context.actorOf(GalleryTraversalActor.props(FAConstants.USER_SCRAPS_TEMPLATE, ScrapGalleyKind))
      galleryScrapperActor ! GalleryTraversalActor.StartTraversingOf(nl)
      scrapScrapperActor ! GalleryTraversalActor.StartTraversingOf(nl)
      context.become(scrapping)
      traverserDoneCount = 0
      for(i <- 0 until Util.Constants.downloaderCount) {
        context.actorOf(Props[DownloadActor]) ! Start
      }
    }
    case s@_ => log.error(s"Unknown msg received $s")
  }


  def scrapping : Actor.Receive = {
    case AskForWork => {
      log.debug("Child asking for more work")
      log.debug(s"Jobqueue size => ${jobQueue.size}")
      log.debug(s"Childqueue size => ${childrenQueue.size}")
      if(jobQueue.isEmpty) childrenQueue.enqueue(sender())
      else sender() ! jobQueue.dequeue()
      tryScheduleSelfRestart()
    }
    case job@GeneratedDownloadJob(_, _, id, _) => {
      log.debug(s"New Job generated, view id ${id}")
      log.debug(s"Jobqueue size => ${jobQueue.size}")
      log.debug(s"Childqueue size => ${childrenQueue.size}")
      if(childrenQueue.isEmpty) jobQueue.enqueue(job)
      else childrenQueue.dequeue() ! job
      tryScheduleSelfRestart()
    }
    case TraverserDone => {
      traverserDoneCount += 1
      tryScheduleSelfRestart()
    }
    case s@_ => log.error(s"Unknown msg received $s")
  }

  def tryScheduleSelfRestart(): Unit = {
    import Util.Constants
    if(!scheduledRestart && traverserDoneCount >= 2 && jobQueue.isEmpty) {
      traverserDoneCount = 0
      scheduledRestart = true
      log.info("Sched restart ")
      childrenQueue.clear()
      jobQueue.clear()
      context.become(receive)
      val timer = new Timer
      timer.schedule(new TimerTask {
        override def run(): Unit =  {
          self ! Start
        }
      }, Constants.restartTime * 1000)
    }
  }
}


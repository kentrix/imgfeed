package actors.fa

import java.io.File
import java.net.URLEncoder
import java.nio.file.Paths
import javax.persistence.EntityManager

import Util.{Constants, PersistenceManager}
import actors.ActorCommons.{AskForWork, Start}
import actors.fa.FAHelper.{NormalGalleryKind, ScrapGalleyKind}
import actors.fa.GalleryTraversalActor.GeneratedDownloadJob
import akka.actor.{Actor, ActorLogging, ActorRef}
import analysis.ImageAnalysisResult
import com.typesafe.config.ConfigFactory
import jpa._

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success}
import scalaz.{-\/, \/-}

class DownloadActor extends Actor with ActorLogging{
  var fileRoot : String = _
  var imageDao : ImageDAO = _
  var parent : ActorRef = _

  override def preStart(): Unit = {
    imageDao = new ImageDAOJPAImpl()
    fileRoot = Constants.fileRoot
  }

  override def receive = {
    case GeneratedDownloadJob(artist, pageData, uid, kind) => {
      import java.io.FileOutputStream
      import context.dispatcher

      log.info(s"Connecting to ${pageData.downloadLink}")
      Future[Unit]{
        val res = FAHelper.performEither(FAHelper.retryJsoupGetRaw(pageData.downloadLink, 10, v => FAHelper.imageAnalysis(v)))
        log.debug(s"Downloading link ${pageData.downloadLink}")
        val name = pageData.downloadLink.split("/").last
        val path = fileRoot + "/" + artist.getUniqueWebsiteId + "/" + name
        val file = new File(path)
        def writeFile() = { new FileOutputStream(file).write(res); log.debug(s"Writing file ${file.getAbsolutePath}") }
        file.getParentFile.mkdirs()
        if(!file.exists()) {
          writeFile()
        }
        else {
          log.warning(s"File ${file.getPath} already exists, checking for integrity")
          if(file.length() < res.length) {
            log.warning("File failed checking, overwritting")
            writeFile()
          }
          else log.warning("File check passed")
        }

        val em = PersistenceManager.getFactory.createEntityManager()
        em.getTransaction.begin()

        val resa = em.createQuery[Artist](s"SELECT a FROM ARTISTS a WHERE a.website = :ws AND a.uniqueWebsiteId = :wid", classOf[Artist])
          .setParameter("ws", artist.getWebsite).setParameter("wid", artist.getUniqueWebsiteId) getResultList

        if (resa.size() != 1) throw new Exception("Artist Not Found")
        val artistP = resa.get(0)
        val image = new Image(artistP, uid, file.getAbsolutePath)
        em.persist(image)
        kind match {
          case NormalGalleryKind => artistP.setLastImage(image)
          case ScrapGalleyKind => artistP.setLastScrapImage(image)
        }
        em.merge(artistP)
        em.getTransaction.commit()
      }.onComplete({
          case Success(()) =>
            if(Constants.downloaderDelay > 0) {
              import scala.concurrent.blocking
              log.debug(s"Sleeping for ${Constants.downloaderDelay}")
              blocking(Thread.sleep(Constants.downloaderDelay * 1000))
            }
            parent ! AskForWork
          case Failure(fail) =>
            log.error(fail.toString)
            parent ! AskForWork
      })

    }

    case Start => {
      parent = sender()
      parent ! AskForWork
    }
  }
}

package actors.fa

object FAConstants {
  val WEBSITE_PREFIX = "https://furaffinity.net/"
  val USER_PREFIX = WEBSITE_PREFIX + "user/"
  val USER_PAGE_TEMPLATE = USER_PREFIX + "%s/"
  val USER_GALLERY_TEMPLATE = WEBSITE_PREFIX + "gallery/%s/%d/?perpage=72"
  val USER_SCRAPS_TEMPLATE = WEBSITE_PREFIX + "scraps/%s/%d/?perpage=72"
  val USER_WATCHLIST_TEMPLATE = WEBSITE_PREFIX + "watchlist/by/%s/%d/"
  val IMAGE_PAGE_TEMPLATE = WEBSITE_PREFIX + "view/%s"
  val GALLEY_KIND = "LASTIMAGE_ID"
  val SCRAP_KIND = "LASTSCRAPIMAGE_ID"
}


package actors.fa

import java.net.URL

import Exceptions.NoLoginException
import akka.actor.Actor
import com.gargoylesoftware.htmlunit.html.{DomNode, DomNodeList, HtmlPage, HtmlTable}
import jpa.Tag
import org.jsoup.nodes.Document

import scala.collection.JavaConverters._
import scalaz.\/
import scalaz.Scalaz._

object ImagePage {
  def parsePage(doc : Document): \/[Exception, PageData] = {
    val elements = doc.select("html > body > div > div#page-submission > table tr td table tr td div b a")
    val dlLinkDomElement = elements.get(1)
    if(!dlLinkDomElement.text().matches("[Dd]ownload")) {
      new NoLoginException().left
    }
    else {
      val dlurl = dlLinkDomElement.attr("abs:href")
      val keywords = doc.getElementById("keywords")
      //val kws: Set[Tag] = keywords.children().asScala.map(x => Tag(x.text())).toSet
      PageData(dlurl, null, "").right
    }
  }

  case class PageData(downloadLink : String, tags : Set[Tag], metaData : String)
}

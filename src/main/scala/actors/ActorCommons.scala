package actors

import java.net.URL

import com.typesafe.config.Config
import jpa.Artist



object ActorCommons {

  trait ActorMsg
  case object Start extends ActorMsg
  case object Stop extends ActorMsg
  case object Interrupt extends ActorMsg
  case class ApplyConfig(conf : Config) extends ActorMsg
  case object Done extends ActorMsg
  case class Error(exception: Exception) extends ActorMsg
  case object AskForWork extends ActorMsg
  case object Recheck extends ActorMsg

}

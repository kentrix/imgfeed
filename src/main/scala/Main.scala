import java.io._
import java.nio.file.Path
import java.util

import Util.PersistenceManager
import actors.ActorCommons.Start
import actors.fa.{FASupervisor, WatchlistTraversalActor}
import akka.actor.{ActorSystem, Props}
import com.typesafe.scalalogging.LazyLogging
import jpa.Image
import org.rogach.scallop.ScallopConf

import scala.collection.JavaConverters._
import scalaz.{-\/, \/-}

object Main extends LazyLogging{
  import Util.Constants
  class Conf(args : Seq[String]) extends ScallopConf(args) {
    val cookieFile = opt[String](required = true)
    val downloadDir = opt[String](default = Some(System.getProperty("user.home") + System.getProperty("file.separator") + "FADL"))
    val databaseFile = opt[String](default = Some(System.getProperty("user.dir") + System.getProperty("file.separator") + "imgfeed"))
    val watchListOf = opt[String](required = true)
    val downloaderCount = opt[Int](default = Some(4))
    val traverserDelay = opt[Int](default = Some(0))
    val downloaderDelay = opt[Int](default = Some(0))
    val restartTime = opt[Int](default = Some(28800))
    val recheck = opt[Boolean](default = Some(false))
    verify()
  }

  def main(args: Array[String]): Unit = {
    val conf = new Conf(args)
    processCookieFile(new File(conf.cookieFile.apply()))
    PersistenceManager.setDbFile(conf.databaseFile.apply())
    Constants.fileRoot = conf.downloadDir.apply()
    Constants.downloaderCount = conf.downloaderCount.apply()
    Constants.watchlistOf = conf.watchListOf.apply()
    Constants.traverserDelay = conf.traverserDelay.apply()
    Constants.downloaderDelay = conf.downloaderDelay.apply()
    Constants.restartTime = conf.restartTime.apply()
    val recheck = conf.recheck.apply()

    if(recheck) {
      import actors.fa._
      var failedRecheck = 0
      var passedRecheck = 0
      var redownloaded = 0
      var failedRedownload = 0
      val manager = PersistenceManager.getFactory createEntityManager()
      manager.getTransaction.begin()
      val query = manager.createQuery[Image](s"SELECT i FROM IMAGES i", classOf[Image])
      query.getResultList.asScala.foreach { e =>
        // Check for file
        val file = new File(Constants.fileRoot + e.getFilePath)
        if(!file.exists()) {
          logger.info(s"${file.toString} Not found, attempting to redownload")
          failedRecheck += 1
          val imagePage = FAConstants.IMAGE_PAGE_TEMPLATE.format(e.getUniqueWebsiteId)
          val pageData = ImagePage.parsePage(FAHelper.retryJsoupGetPage(imagePage))
          pageData match {
            case -\/(s) =>
              logger.error(s"Failed to parse the page ${imagePage}")
              logger.error(s"Unable to download ${e.getUniqueWebsiteId}")
              failedRedownload += 1
            case \/-(pageData) =>
              val res = FAHelper.retryJsoupGetRaw(pageData.downloadLink, 10, v => FAHelper.imageAnalysis(v))
              res match {
                case -\/(s) =>
                  logger.error(s"Unable to download after attempts ${s.toString}")
                  failedRedownload += 1
                case \/-(res) =>
                  logger.info(s"Writting to ${file.toString}")
                  file.setWritable(true)
                  def writeFile() = { new FileOutputStream(file).write(res); logger.debug(s"Writing file ${file.getAbsolutePath}") }
                  file.getParentFile.mkdirs()
                  writeFile()
                  redownloaded += 1
              }
          }
        }
        else {
          passedRecheck += 1
        }
      }

      logger.info(s"Total: ${query.getResultList.size}")
      logger.info(s"Passed: ${passedRecheck}")
      logger.info(s"Failed: $failedRecheck")
      logger.info(s"Redownloaded: $redownloaded")
      logger.info(s"Failed Redownload: $failedRedownload")


    } else {
      val ac = ActorSystem.create()
      val actor = ac.actorOf(Props[FASupervisor])
      actor ! Start
    }
  }

  def processCookieFile(file: File) = {
    // XXX : assertions
    assert(file.canRead)
    val src = scala.io.Source.fromFile(file)
    val str = try src.mkString finally src.close()
    val map = new java.util.HashMap[String, String]()
    str.split("\n").foreach({ s =>
      val ss = s.split(":")
      assert(ss.length == 2)
      map.put(ss.apply(0), ss.apply(1))
    })
    Constants.setCookie(map)
  }

}

package types

import scalaz.\/

object Types {
  type Exceptionable[A] = \/[Exception, A]
  type Failable = \/[Exception, Unit]
}

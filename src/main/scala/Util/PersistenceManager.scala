package Util

import javax.persistence.{EntityManagerFactory, Persistence}

object PersistenceManager {
  private[this] var factory : EntityManagerFactory = _
  private[this] var dbFile : String = _
  def getFactory: EntityManagerFactory = {
    if(factory == null) {
      if(dbFile != null) {
        import java.util
        val persistenceMap = new util.HashMap[String, String]()

        persistenceMap.put("javax.persistence.jdbc.url", s"jdbc:h2:$dbFile;MV_STORE=FALSE")
        persistenceMap.put("javax.persistence.jdbc.user", "")
        persistenceMap.put("javax.persistence.jdbc.password", "")
        persistenceMap.put("javax.persistence.jdbc.driver", "org.h2.Driver")

        factory = Persistence.createEntityManagerFactory(Constants.PERSISTENCE_UNIT, persistenceMap)
      }
    }
    factory
  }

  def setDbFile(string: String): Unit = dbFile = string

}

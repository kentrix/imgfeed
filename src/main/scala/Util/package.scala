package object Exceptions {
  class NoLoginException extends Exception
  class PossibleDisabledAccount extends Exception
}

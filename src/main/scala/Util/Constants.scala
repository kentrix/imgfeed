package Util

final case object Constants {
  val PERSISTENCE_UNIT = "imgfeed"

  var cookie : java.util.Map[String, String] = _
  var fileRoot : String = _
  var downloaderCount : Int = _
  var watchlistOf : String = _
  var traverserDelay : Int = _
  var downloaderDelay : Int = _
  var restartTime: Int = _

  def getCookie() : java.util.Map[String, String] = {
    cookie
  }

  def setCookie(map : java.util.Map[String, String]) = cookie = map

}

package jpa
import java.lang.Exception

import scalaz.Scalaz._
import javax.persistence.EntityManager
import javax.transaction.Transactional

import Util.PersistenceManager
import types.Types.{Exceptionable, Failable}

import scalaz.\/

trait ArtistDAO {
  def newArtist(artist: Artist) : Failable
  def delArtist(artist: Artist) : Failable
  def delArtistById(id : Long) : Failable
  def getArtistById(id : Long) : \/[Exception, Artist]
  def getArtistByWebsiteAndUniqueString(website: Website, uniqueId: String) : \/[Exception, Artist]
  def getByArtist(artist: Artist) : Exceptionable[Artist]
  def destroy() : Unit
}

class ArtistDAOJpaImpl(entityManager: EntityManager) extends ArtistDAO {
  override def newArtist(artist: Artist): Failable = {
    import actors.fa.FAHelper.cleanly
    cleanly[EntityManager, Unit](entityManager)
      { em => ()}
      { em => {
      em.getTransaction.begin()
      em.persist(artist)
      em.getTransaction.commit()
    }
    }
  }

  override def delArtist(artist: Artist): Failable = ???

  override def delArtistById(id: Long): Failable = ???

  override def getArtistById(id: Long): Exceptionable[Artist] = ???

  @Transactional
  override def getArtistByWebsiteAndUniqueString(website: Website, uniqueId: String): Exceptionable[Artist] = {
    import actors.fa.FAHelper.cleanly
    cleanly[EntityManager, Artist](entityManager)
      { em => () }
      { em => {
        em.getTransaction.begin()
        val res = em.createQuery[Artist](s"SELECT a FROM ARTISTS a WHERE a.website = :ws AND a.uniqueWebsiteId = :wid", classOf[Artist])
          .setParameter("ws", website.toString).setParameter("wid", uniqueId) getResultList

        em.getTransaction.commit()

        if (res.size() != 1) throw new Exception("Artist Not Found") else res.get(0)
      }

      }
  }


  @Transactional
  override def getByArtist(artist: Artist): \/[Exception, Artist] = {
    import actors.fa.FAHelper.cleanly
    cleanly[EntityManager, Artist](entityManager)
      { em => () }
      { em => {
        em.getTransaction.begin()
        val res = em.createQuery[Artist](s"SELECT a FROM ARTISTS a WHERE a.website = :ws AND a.uniqueWebsiteId = :wid", classOf[Artist])
          .setParameter("ws", artist.getWebsite).setParameter("wid", artist.getUniqueWebsiteId) getResultList

        em.getTransaction.commit()

        if (res.size() != 1) throw new Exception("Artist Not Found") else res.get(0)
      }

      }
    }

  override def destroy(): Unit = {}
}


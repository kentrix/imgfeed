package jpa

import javax.persistence.{EntityManager, PersistenceContext}
import javax.transaction.Transactional

import scalaz.\/
import Util.PersistenceManager

import scalaz.Scalaz._

trait ImageDAO {
  def getImagesForArtist(artist: Artist) : \/[Exception, Set[Image]]
  def saveImage(image: Image) : \/[Exception, Unit]
  def getImagesForTagsConjunction(tags : Tag*) : \/[Exception, Set[Image]]
  def getImagesForTagsDisjunction(tags : Tag*) : \/[Exception, Image]
  def addTagsForImage(tags : Tag*) : \/[Exception, Unit]
  def deleteTagsForImage(tags : Tag*) : \/[Exception, Unit]
}
class ImageDAOJPAImpl() extends ImageDAO {
  override def getImagesForArtist(artist: Artist) = ???

  @Transactional
  override def saveImage(image: Image) = {
    import actors.fa.FAHelper.cleanly
    cleanly[EntityManager, Unit](PersistenceManager.getFactory.createEntityManager())
      { em => em.close() }
      { em => {
        em.getTransaction.begin()
        em.persist(image)
        em.getTransaction.commit()
        ().right
      }
      }
  }

  override def getImagesForTagsConjunction(tags: Tag*) = ???

  override def getImagesForTagsDisjunction(tags: Tag*) = ???

  override def addTagsForImage(tags: Tag*) = ???

  override def deleteTagsForImage(tags: Tag*) = ???
}

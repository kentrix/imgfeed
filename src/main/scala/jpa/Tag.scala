package jpa

case class Tag(tag: String) {
  private val theTag = tag.trim.toLowerCase()
  override def equals(that : Any): Boolean = that match {
    case t@Tag(_) => t.theTag equals this.theTag
    case _ => false
  }

  override def toString: String = theTag
}
